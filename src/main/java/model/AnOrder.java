package model;

public class AnOrder {
	
	private int orderId;
	private String clientFirstName;
	private String clientLastName;
	private String product;
	private int quantity;
	
	public AnOrder(String clientFirstName, String clientLastName, String product, int quantity) {
		super();
		this.clientFirstName = clientFirstName;
		this.clientLastName = clientLastName;
		this.product = product;
		this.quantity = quantity;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getClientFirstName() {
		return clientFirstName;
	}

	public void setClientFirstName(String clientFirstName) {
		this.clientFirstName = clientFirstName;
	}

	public String getClientLastName() {
		return clientLastName;
	}

	public void setClientLastName(String clientLastName) {
		this.clientLastName = clientLastName;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
}
