package model;

public class OrderTotal {

	private int id;
	private int clientId;
	private double totalCost;
	
	public OrderTotal(int clientId, double totalCost) {
		super();
		this.clientId = clientId;
		this.totalCost = totalCost;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
}
