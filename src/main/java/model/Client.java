package model;

public class Client {
	
	private int clientId;
	private String firstName;
	private String lastName;
	private String city;
	
	public Client(String firstN, String lastN, String city) {
		this.firstName = firstN;
		this.lastName = lastN;
		this.city = city;
	}
	
	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}