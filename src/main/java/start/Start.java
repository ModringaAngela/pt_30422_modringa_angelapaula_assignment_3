package start;
import businessLogic.ClientBLL;
import businessLogic.OrderBLL;
import businessLogic.OrderTotalBLL;
import businessLogic.ProductBLL;
import presentation.FileParser;
import model.Product;
import presentation.CommandsCaller;
import model.AnOrder;
import model.Client;
import model.OrderTotal;
/**
 * The class for starting the application that contains the main method
 * @author angel
 *
 */
public class Start {
		
	/**
	 * Method that calls the class that parse the input file given as a command line arrgument
	 * @param args
	 */
	public static void main(String[] args) {
		
		FileParser parser = new FileParser(args[0]);
	}
}
