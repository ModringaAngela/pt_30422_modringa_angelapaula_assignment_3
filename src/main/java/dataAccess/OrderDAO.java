package dataAccess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.AnOrder;

/**
 * Class used to access the data from AnOrder table
 * @author angel
 *
 */
public class OrderDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO anOrder (clientFirstName, clientLastName, product, quantity)"
			+ " VALUES (?,?,?,?)";
	private final static String reportStatementString = "SELECT * FROM anOrder";
	
	/**
	 * creates a new record in AnOrder table
	 * @param order: AnOrder
	 */
	public static void createOrder(AnOrder order) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, order.getClientFirstName());
			insertStatement.setString(2, order.getClientLastName());
			insertStatement.setString(3, order.getProduct());
			insertStatement.setInt(4, order.getQuantity());
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionToDB.closeStatement(insertStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
	}
	
	/**
	 * 
	 * @return orders : ArrayList containing all orders
	 */
	public static ArrayList<AnOrder> retrieveAllOrders() {

		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement reportStatement = null;
		ResultSet result = null;
		ArrayList<AnOrder> orders = new ArrayList<AnOrder>();
			try {
				reportStatement = dbConnection.prepareStatement(reportStatementString);
				result = reportStatement.executeQuery();
				while(result.next()) {
					String firstName = result.getString("clientFirstName");
					String lastName = result.getString("clientLastName");
					String product = result.getString("product");
					int quantity = result.getInt("quantity");
					orders.add(new AnOrder(firstName, lastName, product, quantity));		
				}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"OrderDAO:" + e.getMessage());
			} finally {
				ConnectionToDB.closeResultSet(result);
				ConnectionToDB.closeStatement(reportStatement);
				ConnectionToDB.closeConnection(dbConnection);
			}
		return orders;
	}
}
