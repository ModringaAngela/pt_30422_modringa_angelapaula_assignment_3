package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 * Class used to access the data from the Product table
 * @author angel
 *
 */
public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (productName, quantity, price)"
			+ " VALUES (?,?,?)";
	private final static String reportStatementString = "SELECT * FROM product";
	private final static String deleteStatementString = "DELETE FROM product WHERE productName = ?";
	private final static String findStatementString = "SELECT * FROM product where productName = ?";
	private final static String findStatementString2 = "SELECT quantity FROM product where productName = ?";
	private final static String updateStatementString = "UPDATE product SET quantity = ? where productName = ?";
	
	/**
	 * Search the product by its name in the table
	 * @param productName: String
	 * @return An object of type Product if it s found, null otherwise
	 */
	public static Product findProductByName(String productName) {
		
		Product productSearched = null;
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setString(1, productName);
			result = findStatement.executeQuery();
			if(result.next() == false) 
				return null;
			String product = result.getString("productName");
			int quantity = result.getInt("quantity");
			double price = result.getDouble("price");
			productSearched = new Product(product, quantity, price);
		
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Checking if the product exists" +  e.getMessage());
		}
		
		finally {
			ConnectionToDB.closeConnection(dbConnection);
			ConnectionToDB.closeResultSet(result);
			ConnectionToDB.closeStatement(findStatement);
		}
		return productSearched;
	}
	
	/**
	 * Returns the quantity of the product passed as argument by name, or 0 if the product is not available at all
	 * @param productName
	 * @return: integer
	 */
	public static int availableQuantity(String productName) {
		
		int availableQuantity = 0;
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString2);
			findStatement.setString(1, productName);
			result = findStatement.executeQuery();
			result.next();
			availableQuantity = result.getInt("quantity");
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING,"Checking the available quantity:" + e.getMessage());
		}
		finally {
			ConnectionToDB.closeConnection(dbConnection);
			ConnectionToDB.closeResultSet(result);
			ConnectionToDB.closeStatement(findStatement);
		}
		return availableQuantity;
	}
	
	/**
	 * Update the stock of the product whose name is given
	 * @param product: Product, a reference to the object of type product
	 * @param quantity: the new quantity that will be available for the product
	 */
	public static void updateStock(Product product, int quantity) {
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1,  quantity);
			updateStatement.setString(2, product.getProductName());
			updateStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Update the Stock: " + e.getMessage());
		} finally {
			ConnectionToDB.closeStatement(updateStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
	}
	
	/**
	 * Insert a new product in the table
	 * @param product: Product
	 */
	public static void insert(Product product) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getProductName());
			insertStatement.setInt(2, product.getQuantity());
			insertStatement.setDouble(3, product.getPrice());
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO: insert a new product " + e.getMessage());
		} finally {
			ConnectionToDB.closeStatement(insertStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
	}

	/**
	 * It deletes a product from the database based on the name
	 * If more products have the same name, they will all be deleted
	 * @param productName
	 */
	public static void deleteProduct(String productName) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement deleteStatement = null;
			try {
				 deleteStatement = dbConnection.prepareStatement(deleteStatementString);
				 deleteStatement.setString(1, productName);
				 deleteStatement.execute();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ProductDAO: delete product " + e.getMessage());
			} finally {
				ConnectionToDB.closeStatement(deleteStatement);
				ConnectionToDB.closeConnection(dbConnection);
			}
	}
	
	/**
	 * It returns a list containing all products 
	 * 
	 * @return
	 */
	public static ArrayList<Product> retrieveAllProducts() {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement reportStatement = null;
		ResultSet result = null;
		ArrayList<Product> products = new ArrayList<Product>();
		
		try {
			reportStatement = dbConnection.prepareStatement(reportStatementString);
			result = reportStatement.executeQuery();
			while(result.next()) {
				String product = result.getString("productName");
				int quantity = result.getInt("quantity");
				double price = result.getDouble("price");
				products.add(new Product(product, quantity, price));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO: " + e.getMessage());
		} finally {
			ConnectionToDB.closeResultSet(result);
			ConnectionToDB.closeStatement(reportStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
		return products;
	}
}
