package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.OrderTotal;
import model.Product;

public class OrderTotalDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO orderTotal (clientId, totalCost)"
			+ " VALUES (?,?)";
	private final static String reportStatementString = "SELECT * FROM orderTotal";
	private final static String updateStatementString = "UPDATE orderTotal SET totalCost = ? where clientId = ?";
	private final static String findStatementString = "SELECT * FROM orderTotal where clientId = ?";
	
	public static OrderTotal findOrderByClientId(int id) {
		OrderTotal total = null;
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement findStatement = null;
		ResultSet result = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, id);
			result = findStatement.executeQuery();
			if(result.next() == false) 
				return null;
			int clientId = result.getInt("clientId");
			double cost = result.getDouble("totalCost");
			total = new OrderTotal(clientId, cost);
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Check if the client already has a record" +  e.getMessage());
		}
		
		finally {
			ConnectionToDB.closeConnection(dbConnection);
			ConnectionToDB.closeResultSet(result);
			ConnectionToDB.closeStatement(findStatement);
		}
		return total;
	}
	
	public static void createOrderTotal(OrderTotal total) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString);
			insertStatement.setInt(1, total.getClientId());
			insertStatement.setDouble(2, total.getTotalCost());
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderTotalDAO:create " + e.getMessage());
		} finally {
			ConnectionToDB.closeStatement(insertStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
	}
	
	public static void update(int clientId, double price) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(updateStatementString);
			insertStatement.setDouble(1, price);
			insertStatement.setInt(2, clientId);
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderTotalDAO:update " + e.getMessage());
		} finally {
			ConnectionToDB.closeStatement(insertStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
		
	}
	
	/**
	 * 
	 * @return orders : ArrayList containing all orders
	 */
	public static ArrayList<OrderTotal> retrieveTotalForEachClient() {

		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement reportStatement = null;
		ResultSet result = null;
		ArrayList<OrderTotal> orders = new ArrayList<OrderTotal>();
			try {
				reportStatement = dbConnection.prepareStatement(reportStatementString);
				result = reportStatement.executeQuery();
				while(result.next()) {
					int clientId = result.getInt("clientId");
					double total = result.getInt("totalCost");
					orders.add(new OrderTotal(clientId, total));		
				}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"OrderDAO:" + e.getMessage());
			} finally {
				ConnectionToDB.closeResultSet(result);
				ConnectionToDB.closeStatement(reportStatement);
				ConnectionToDB.closeConnection(dbConnection);
			}
		return orders;
	}
	
}
