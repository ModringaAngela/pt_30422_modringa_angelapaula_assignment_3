package dataAccess;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.*;
import model.Client;
/**
 * Class used to access the data from Client table
 * @author angel
 *
 */
public class ClientDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (firstName, lastName, city)"
			+ " VALUES (?,?,?)";
	private final static String reportStatementString = "SELECT * FROM client";
	private final static String deleteStatementString = "DELETE FROM client WHERE firstName = ? AND lastName = ? AND city = ?";
	private final static String findStatementString = "SELECT * FROM client where firstName = ? AND lastName = ? AND city = ?";
	private final static String findByNameStatementString = "SELECT * FROM client where firstName = ? AND lastName = ?";
	/**
	 * Search for the client by full name and city if the city parameter is not null
	 * @param firstName
	 * @param lastName
	 * @param city
	 * @return an object of type Client if the client exists, null otherwise
	 */
	public static Client findClient(String firstName, String lastName, String city) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement findStatement1 = null;
		PreparedStatement findStatement2 = null;
		ResultSet result = null;
		Client client = null;
		
		try {
			if(city != null) {
				findStatement1 = dbConnection.prepareStatement(findStatementString);
				findStatement1.setString(1, firstName);
				findStatement1.setString(2, lastName);
				findStatement1.setString(3, city);
				result = findStatement1.executeQuery();
			}
			else {
				findStatement2 = dbConnection.prepareStatement(findByNameStatementString);
				findStatement2.setString(1, firstName);
				findStatement2.setString(2, lastName);
				result = findStatement2.executeQuery();
			}
			
			if(result.next() == false) 
				return null;
			client = new Client(result.getString("firstName"), result.getString("lastName"), result.getString("city"));
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO: find by name " + e.getMessage());
		}
		finally {
			ConnectionToDB.closeConnection(dbConnection);
			ConnectionToDB.closeResultSet(result);
			ConnectionToDB.closeStatement(findStatement1);
			ConnectionToDB.closeStatement(findStatement2);
		}
		return client;
	}
	
	
	
	/**
	 * Insert a new client in the table
	 * @param client: Client
	 */
	public static void insert(Client client) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getFirstName());
			insertStatement.setString(2, client.getLastName());
			insertStatement.setString(3, client.getCity());
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO: insert " + e.getMessage());
		} finally {
			ConnectionToDB.closeStatement(insertStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
	}
	
	/**
	 * Returns a list of all clients
	 * @return ArrayList of objects of type Client
	 */
	public static ArrayList<Client> retrieveAllClients() {

		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement reportStatement = null;
		ResultSet result = null;
		ArrayList<Client> clients = new ArrayList<Client>();
		
		try {
			reportStatement = dbConnection.prepareStatement(reportStatementString);
			result = reportStatement.executeQuery();
			while(result.next()) {
				String firstName = result.getString("firstName");
				String lastName = result.getString("lastName");
				String city = result.getString("city");
				clients.add(new Client(firstName, lastName, city));
				
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO " + e.getMessage());
		} finally {
			ConnectionToDB.closeResultSet(result);
			ConnectionToDB.closeStatement(reportStatement);
			ConnectionToDB.closeConnection(dbConnection);
		}
		return clients;
	}
	
	/**
	 * Delete a client referred by name
	 * @param clientName: String
	 */
	public static void deleteClient(String clientFirstName, String clientLastName, String city) {
		
		Connection dbConnection = ConnectionToDB.getInstanceOfConnection();
		PreparedStatement deleteStatement = null;
			try {
				deleteStatement = dbConnection.prepareStatement(deleteStatementString);
				 deleteStatement.setString(1, clientFirstName);
				 deleteStatement.setString(2, clientLastName);
				 deleteStatement.setString(3, city);
				deleteStatement.execute();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
			} finally {
				ConnectionToDB.closeStatement(deleteStatement);
				ConnectionToDB.closeConnection(dbConnection);
			}
	}
}
