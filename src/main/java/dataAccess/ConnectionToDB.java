package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Statement;

public class ConnectionToDB {
	private static final Logger LOGGER = Logger.getLogger(ConnectionToDB.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/warehouse";
	private static final String UID = "root";
	private static final String PASSWORD = "ciocolata02";
	
	private static ConnectionToDB instance = new ConnectionToDB();
	
	private ConnectionToDB() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException: " +e);
		}
	}
	
	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL, UID, PASSWORD);
		}catch(SQLException e) {
			LOGGER.log(Level.SEVERE, "An error occured while trying to make the connection");
			//System.err.println("SQLException: " + e);
	        //System.exit(1);
		}
		//System.out.println("Connection created");
		return connection;
	}
	
	public static Connection getInstanceOfConnection() {
		return instance.createConnection();
	}
	
	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				//System.err.println("SQLException: " + e);
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
		//System.out.println("Connection closed");
	}
	
	public static void closeResultSet(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				//System.err.println("SQLException: " + e);
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}
	
	public static void closeStatement(Statement statement) {
		if(statement != null) {
			try {
				statement.close();
			}catch(SQLException e){
				//System.err.println("SQLException: " + e);
				LOGGER.log(Level.WARNING, "An error occured while trying to close the Statement");
			}
		}
	}
}

