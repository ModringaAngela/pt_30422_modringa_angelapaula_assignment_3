package businessLogic;
import java.util.ArrayList;

import dataAccess.OrderTotalDAO;
import dataAccess.ProductDAO;
import model.Client;
import model.AnOrder;
import model.OrderTotal;

public class OrderTotalBLL {

	public void generateTotalForEachClient(ArrayList<Client> clients, ArrayList<AnOrder> orders) {
		for(Client c: clients) {
			OrderTotal total = OrderTotalDAO.findOrderByClientId(c.getClientId());
			if(total == null)
				OrderTotalDAO.createOrderTotal(new OrderTotal(c.getClientId(), 0));
			for(AnOrder o : orders) {
				OrderTotalDAO.update(c.getClientId(), ProductDAO.findProductByName(o.getProduct()).getPrice());
			}
		}	
	}
	
	public ArrayList<OrderTotal> retrieveTotalForEaschClient(){
		return OrderTotalDAO.retrieveTotalForEachClient();
	}
}
