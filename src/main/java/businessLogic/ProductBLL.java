package businessLogic;
import bll.validators.*;
import dataAccess.ProductDAO;
import java.util.List;
import java.util.ArrayList;
import model.Product;


/**
 * Class used to implement the application logic while working with the table Product from the warehouse database
 * @author angel
 *
 */
public class ProductBLL {
	
	private List <Validator<Product>> validators;
	
	/**
	 * Constructor that add the validators used for checking the fields
	 */
	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		validators.add(new PriceValidator(0.01f));
		validators.add(new QuantityValidator());
	}
	
	/**
	 * Returns a reference to an object of type Product that has the name given as parameter
	 * @param productName: String
	 * @return: Product
	 */
	public static Product findProductByName(String productName) {
		return ProductDAO.findProductByName(productName);
	}
	
	/**
	 * This method first verifies if the price and the quantity of the product to be inserted are valid.
	 * If they are not, an exception is thrown by the validate() method.
	 * Then it is verified if the product exists already in the table (same name and price). If so, the quantity is updated (sum of already available quantity and the new one)
	 * Otherwise, the product is inserted;
	 * @param productToBeInserted: Product
	 */
	public void insertProduct(Product productToBeInserted) {
		for(Validator<Product> v : validators)
			v.validate(productToBeInserted);
		Product existentProduct = findProductByName(productToBeInserted.getProductName());
		if(existentProduct != null) {
			if(existentProduct.getPrice() == productToBeInserted.getPrice()) {
				int newQuantity = existentProduct.getQuantity() + productToBeInserted.getQuantity();
				ProductDAO.updateStock(existentProduct, newQuantity);
			}
		}
			
		else {
			ProductDAO.insert(productToBeInserted);
		}
	}
	
	/**
	 * Used for orders. If this method returns false, it means that the product is not available, so it can't be ordered!
	 * @param productName: String
	 * @return true if the product is in stock, false otherwise
	 */
	public static boolean existentProduct(String productName) {
		Product searchedProduct = ProductDAO.findProductByName(productName);
		if(searchedProduct == null)
			return false;
		return true;
	}
	
	/**
	 * deletes a product form the table
	 * @param productName
	 * @throws IllegalArgumentException thrown if there is no product with the name given as an argument in the product table
	 */
	public void deleteProduct(String productName) throws IllegalArgumentException {
		if(!existentProduct(productName))
			throw new IllegalArgumentException("ERROR! The product you want to delete doesn't exist");
		ProductDAO.deleteProduct(productName);
	}
		
	 /** Returns a list containing all products from the table
	  * @return ArrayList
	  */
	public ArrayList<Product> retrieveAllProducts() {
		return ProductDAO.retrieveAllProducts();
	}
	
	/**
	 * Update the stock of the product passed as argument by name
	 * If more products have the same name, the quantity of all will be updated
	 * @param product: Product
	 * @param newQuantity: integer, new quantity of the product that s in stock 
	 */
	public static void updateStock(Product product, int newQuantity) {
		ProductDAO.updateStock(product, newQuantity);
	}
}
