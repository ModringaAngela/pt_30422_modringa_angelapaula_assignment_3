package businessLogic;

import java.util.ArrayList;
import dataAccess.ClientDAO;
import model.Client;

/**
 *  Class used to implement the application logic while working with the table Client from the warehouse database
 */
public class ClientBLL {
	
	/**
	 * Insert a new client in the database. It doesn't check if there already exists a client with the same name and city as the one that
	 * will be added, because in a city could be more people with the same name. Clients are uniquelly identified by id.
	 * @param client
	 */
	public void insertClient(Client client) {
		ClientDAO.insert(client);
	}

	/**
	 * Check if the client exists for the delete operation
	 * @param firstName
	 * @param lastName
	 * @param city
	 * @return true if it exists, false otherwise
	 */
	public static boolean existentClient(String firstName, String lastName, String city) {
		Client client = ClientDAO.findClient(firstName, lastName, city);
		if(client == null)
			return false;
		return true;
	}
	
	/**
	 * Deletes the client identifies by full name and city, verifying first if it exists
	 * @param firstName
	 * @param lastName
	 * @param city
	 * @throws IllegalArgumentException if the client doesn t exist
	 */
	public void deleteClient(String firstName, String lastName, String city) throws IllegalArgumentException{
		if(!existentClient(firstName, lastName, city))
			throw new IllegalArgumentException("ERROR! The client you want to delete doesn't exist");
		ClientDAO.deleteClient(firstName, lastName, city);
	}

	/**
	 * Returns all records from then Client table
	 * @return
	 */
	public ArrayList<Client> retrieveAllClients(){
		return ClientDAO.retrieveAllClients();
	}
}
