package businessLogic;

import java.util.ArrayList;

import dataAccess.ClientDAO;
import dataAccess.OrderDAO;
import dataAccess.ProductDAO;
import model.*;

/**
 *  Class used to implement the application logic while working with the table AnOrder from the warehouse database
 * @author angel
 *
 */
public class OrderBLL {
	
	/**
	 * Calls the method from OrderDAO that creates a new record in AnOrder table, after an object of type AnOrder is created.
	 * If the client or product doesn't exist, or there are not enough products on stock, an exception will be thrown.
	 * If the order is done, the stock is updated.
	 * @param clientFirstName: String
	 * @param clientLastName: String
	 * @param productName: String 
	 * @param quantity: integer
	 */
	public void createOrder(String clientFirstName, String clientLastName, String productName, int quantity)throws IllegalArgumentException, Exception {
		Client client = ClientDAO.findClient(clientFirstName, clientLastName, null);
		Product product = ProductDAO.findProductByName(productName);
		if(client == null)
			throw new IllegalArgumentException("ERROR! The order can't be made because the client "+ clientFirstName + " " + clientLastName + " doesn't exist!");
		if(product == null)
			throw new IllegalArgumentException("ERROR! The order can't be made because the product " + productName + " doesn't exist!");
		if(product.getQuantity() < quantity)
			throw new Exception("ERROR! The quantity " + clientFirstName + " " + clientLastName + " wants to order of " + productName +" is not available!");
		
		AnOrder newOrder = new AnOrder(clientFirstName, clientLastName, productName, quantity);
		OrderDAO.createOrder(newOrder);
		Product productOrdered = ProductBLL.findProductByName(productName);
		ProductBLL.updateStock(productOrdered, productOrdered.getQuantity() - quantity);
	}
	
	/**
	 * Returns a list of all orders from AnOrder table 
	 * @return: a list of orders
	 */
	public ArrayList<AnOrder> retrieveAllOrders() {
		return OrderDAO.retrieveAllOrders();
	}
}
