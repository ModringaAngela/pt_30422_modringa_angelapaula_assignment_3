package bll.validators;
import model.Product;

/**
 * This class is used to check if the price introduced for one product is a number grater than 0.
 * A product with the price 0 won't make sense for our warehouse, and the minimum price needs to be specified, depending on the products.
 * @author angel
 *
 */
public class PriceValidator implements Validator<Product> {
	
	private float minimumPrice;
	
	/**
	 * Constructor of class that sets the mimimum price a product must have
	 * @param minimumPrice
	 */
	public PriceValidator(float minimumPrice){
		this.minimumPrice = minimumPrice;
	}
	
	@Override
	/**
	 * It throws an exception if the price of the product introduced in the database is less than the minimum price allowed
	 */
	public void validate(Product product) {
		if(product.getPrice() < minimumPrice)
			throw new IllegalArgumentException("The price must be grater than 0!");
	}

}
