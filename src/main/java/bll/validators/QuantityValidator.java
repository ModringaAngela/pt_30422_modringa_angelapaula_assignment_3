package bll.validators;
import model.Product;
/**
 * This class is used to check if the quantity introduced for one product is a number grater or equal to 1
 * The quantity must be an integer number, and be at least 1.
 * @author angel
 *
 */
public class QuantityValidator implements Validator<Product> {
	
	/**
	 * It throws an exception if the quantity of the product introduced in the database is less than 1
	 */
	public void validate(Product product) {
		if (product.getQuantity() < 1)
			throw new IllegalArgumentException("The quantity can't be negative!");
	}

}
