package bll.validators;
/**
 * Interface used to validate some data that are going to be introduced in the database
 * The parameter T for now can be only Product, but in the future implementations we may want to check other informations (for example about the client)
 * @author angel
 *
 * @param <T>
 */
public interface Validator<T> {
	/**
	 * method used to validate input data
	 * @param t
	 */
	public void validate(T t);
}
