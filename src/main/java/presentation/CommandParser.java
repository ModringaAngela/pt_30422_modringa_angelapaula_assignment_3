package presentation;

import dataAccess.*;
import model.*;

public class CommandParser {
	
	private String commandString;
	private CommandsCaller call = new CommandsCaller();
	private static int indexProductReport = 1;
	private static int indexClientReport = 1;
	private static int indexOrderReport = 1;
	
	/**
	 * Constructor that assign the given string to the parameter commandString.
	 * @param commandString
	 */
	public CommandParser(String commandString) {
		this.commandString = commandString;
	}
	
	/**
	 * First additional characters as : or , are eliminated. Then the string is split in words (having " " as delimiters).
	 * Then depending on the first word of the command and optionally the second, different methods are called.
	 */
	public void parseCommand() {
		
		commandString = commandString.replace(":", "");
		commandString = commandString.replace(",", "");
		String[] words = commandString.split(" ");
		
		if(words[0].equals("Report")) {
			if(words[1].equals("client")) {
				call.reportClient(indexClientReport, "Report of clients");
				indexClientReport++;
			}
			if(words[1].equals("order")) {
				call.reportOrder(indexOrderReport, "Report of orders");
				indexOrderReport++;
			}
			if(words[1].equals("product")) {
				call.reportProduct(indexProductReport, "Report of products");
				indexProductReport++;
			}
		}
		
		if(words[0].equals("Insert")) {
			if(words[1].equals("client"))
				call.insertClient(words[2], words[3], words[4]);
			if(words[1].equals("product"))
				call.insertProduct(words[2], Integer.valueOf(words[3]), Double.valueOf(words[4]));
		}
		
		if(words[0].equals("Delete")) {
			if(words[1].equals("client"))
				call.deleteClient(words[2], words[3], words[4]);
			if(words[1].equals("Product"))
				call.deleteProduct(words[2]);
		}
		
		if(words[0].equals("Order")) {
			call.createOrder(words[1], words[2], words[3], Integer.valueOf(words[4]));
		}
	}
	
}
