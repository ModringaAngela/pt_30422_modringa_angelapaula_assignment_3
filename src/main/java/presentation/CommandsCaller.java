package presentation;

import model.Product;
import model.Client;
import model.AnOrder;
import businessLogic.ProductBLL;
import businessLogic.ClientBLL;
import businessLogic.OrderBLL;

import java.util.ArrayList;

public class CommandsCaller {
	
	private static final ProductBLL productBll =  new ProductBLL();
	private static final OrderBLL orderBll =  new OrderBLL();
	private static final ClientBLL clientBll =  new ClientBLL();
	private PdfGenerator pdfGenerator;
	private static int index = 1;
	
	/**
	 * Generates a report of the table Client.
	 * Index will be part of the generated file name, to ensure the generation of multiple files
	 * @param index: integer
	 * @param title: String
	 */
	public void reportClient(int index, String title) {
		ArrayList <Client> clients = clientBll.retrieveAllClients();
		this.pdfGenerator = new PdfGenerator<Client>();
		Client client = clients.get(0);
		pdfGenerator.createReportPdf((Object)client, clients, index, title, "Client-Report");
	}
	
	/**
	 * Generates a report of the table Product.
	 * Index will be part of the generated file name, to ensure the generation of multiple files
	 * @param index: integer
	 * @param title: String
	 */
	public void reportProduct(int index, String title) {
		ArrayList<Product> products = productBll.retrieveAllProducts();
		this.pdfGenerator = new PdfGenerator<Product>();
		Product prod = products.get(0);
		pdfGenerator.createReportPdf((Object)prod, products, index, title, "Product-Report");
	}
	
	/**
	 * Generates a report of the table AnOrder.
	 * Index will be part of the generated file name, to ensure the generation of multiple files
	 * @param index: integer
	 * @param title: String
	 */
	public void reportOrder(int index, String title) {
		ArrayList <AnOrder> orders = orderBll.retrieveAllOrders();
		this.pdfGenerator = new PdfGenerator<AnOrder>();
		AnOrder order = orders.get(0);
		pdfGenerator.createReportPdf((Object)order, orders, index, title, "Order-Report");
	}
	
	/**
	 * Creates a new object of type Client and call the insertClient method of clientBll
	 * @param firstName: String
	 * @param lastName: String
	 * @param city: String
	 */
	public void insertClient(String firstName, String lastName, String city) {
		Client newClient = new Client(firstName, lastName, city);
		clientBll.insertClient(newClient);
	}
	
	/**
	 * Creates a new object of type Product and call the insertProduct method productBll
	 * @param productName: String
	 * @param quantity: integer
	 * @param price: double
	 */
	public void insertProduct(String productName, int quantity, double price) {
		Product productToBeInserted = new Product(productName, quantity, price);
		productBll.insertProduct(productToBeInserted);
	}
	
	
	/**
	 * Delete client referred by full name and city. If the client don't exist, an error message is shown.
	 * @param firstName: String
	 * @param lastName: String
	 * @param city: String
	 */
	public void deleteClient(String firstName, String lastName, String city) {
		try {
			clientBll.deleteClient(firstName, lastName, city);
		}catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Delete a product referred by name. If the product don t exist, a message is shown.
	 * @param productName
	 */
	public void deleteProduct(String productName) {
		try {
			productBll.deleteProduct(productName);
		}catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	/**
	 * Creates a new record in the AnOrder table and generates a bill or an error PDF
	 * @param clientFirstName
	 * @param clientLastName
	 * @param productName
	 * @param quantity
	 */
	public void createOrder(String clientFirstName, String clientLastName, String productName, int quantity) {
		this.pdfGenerator = new PdfGenerator<AnOrder>();
		try {
			orderBll.createOrder(clientFirstName, clientLastName, productName, quantity);
			double price = ProductBLL.findProductByName(productName).getPrice();
			double cost = price * quantity;
			pdfGenerator.createBillPdf(clientFirstName, clientLastName, cost, index);
			index++;
		}catch(IllegalArgumentException ex) {
			System.out.println(ex.getMessage());
		}catch(Exception ex) {
			pdfGenerator.createErrorPdf(ex.getMessage());
		}
	}
	
	public void createTotalForClient(Client client) {
		
	}
}
