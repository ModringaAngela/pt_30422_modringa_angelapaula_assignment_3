package presentation;
import java.io.FileOutputStream;
import java.util.ArrayList;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.lang.reflect.Field;

/**
 * Generate a PDF file for the report of one of the tables, for the error message or for the bill
 * @author angel
 *
 * @param <T>: the type of object reported
 */
public class PdfGenerator<T>
{
	
	Document document;
	
	/**
	 * Constructor
	 */
	public PdfGenerator() {
		this.document = new Document();
	}
	
	/**
	 * Creates cells containing the name of the rows from the table
	 * @param table
	 * @param object
	 */
	public void addTableHeader(PdfPTable table, Object object) {
		Field[] fields = object.getClass().getDeclaredFields(); 
		int length = fields.length;
	    for(int i = 1 ; i < length ; i++ ) {
	    	fields[i].setAccessible(true);
	    	  PdfPCell header = new PdfPCell();
		        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
		        header.setBorderWidth(2);
		        header.setPhrase(new Phrase(fields[i].getName()));
		        table.addCell(header);
	    }
	}
	
	/**
	 * add rows to a table, using the reflection technique 
	 * @param table
	 * @param object
	 */
	private void addRows(PdfPTable table, Object object) {
		
		Field[] fields = object.getClass().getDeclaredFields(); 
		
		for(int i = 1 ; i < fields.length ; i++) {
			fields[i].setAccessible(true);
			try {
				table.addCell(String.valueOf(fields[i].get(object)));
			}catch(IllegalAccessException e) {
				System.out.println(e.getMessage());
			}
		}
		
	}
	
	/**
	 * Create a pdf with a report of one of the tables, in a tabular form
	 * @param objectReported
	 * @param results
	 * @param index: to be able to generate multiple pdfs for the same table
	 * @param titleString: title of the pdf
	 * @param name: pdf name
	 */
	public void createReportPdf(Object objectReported, ArrayList<Object> results, int index, String titleString, String name) {
		try {
			PdfWriter.getInstance(document, new FileOutputStream(name + String.valueOf(index) + ".pdf"));
			
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER, 30, BaseColor.BLACK);
			Paragraph paragraph = new Paragraph("                                         ");
			Chunk title = new Chunk(titleString, font);
			PdfPTable table = new PdfPTable(objectReported.getClass().getDeclaredFields().length-1);
			document.add(title);
			document.add(paragraph);
			
			addTableHeader(table, objectReported);
			for(Object o : results)
				addRows(table, o);	
			
			document.add(table);
			document.close();

		}catch(Exception e) {
			System.out.println("Exception" + e.getMessage());
		}
	}
	
	/**
	 * Creates a pdf with a message
	 * @param message
	 */
	public void createErrorPdf(String message) {
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Error.pdf"));
			document.open();
			Chunk text = new Chunk(message);
			document.add(text);
			document.close();
		}catch(Exception e) {
			System.out.println("Exception" + e.getMessage());
		}
	}

	/**
	 * Generates a pdf for a bill
	 * @param clientFirstName
	 * @param clientLastName
	 * @param cost
	 */
	public void createBillPdf(String clientFirstName, String clientLastName, double cost, int index) {
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Bill-" + index +".pdf"));
			document.open();
			Chunk text= new Chunk("Client: "+ clientFirstName + " " + clientLastName + ". Total cost:" + cost);
			document.add(text);
			document.close();
		}catch(Exception e) {
			System.out.println("Exception" + e.getMessage());
		}
	}
}
