package presentation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileParser {

	public FileParser(String fileName) {
		int ct = 1;
		BufferedReader myReader = null;
		try {
			
			FileReader inputFile = new FileReader(fileName);
		    myReader = new BufferedReader(inputFile);
			String command;
			while((command = myReader.readLine()) != null) {
				CommandParser parser = new CommandParser(command);
				parser.parseCommand();
			}
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}finally {
			if(myReader != null)
				try {
					myReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			System.out.println("Done!");
		}
	}
}
